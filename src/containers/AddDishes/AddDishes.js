import React, {useState} from 'react';
import Form from "../../components/Form/Form";
import {addDishes} from "../../store/actions/dishesAction";
import {useDispatch} from "react-redux";

const AddDishes = (props) => {

    const dispatch = useDispatch();

    // const dishes = useSelector(state => state.dishes);

    const [post, setPost] = useState({
        name: '',
        price: '',
        image: '',
    });


    const postDish = (e) => {
        e.preventDefault();
        dispatch(addDishes(post, props.history));
    };

    const onChangeDish = (e) => {
        const {name, value} = e.target;
        setPost(prevState => ({
            ...prevState,
            [name]: value
        }))
    }


    return (
        <>
            <Form
                onsubmit={postDish}
                onchange={onChangeDish}
                post={post}
            />
        </>
);
};

export default AddDishes;