import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getOne, editDish} from "../../store/actions/dishesAction";
import Form from "../../components/Form/Form";

const EditDishes = (props) => {

    const id = props.match.params.id
    const dispatch = useDispatch();
    const oneDish = useSelector(state => state.dishes.oneDish);

    const [edit, setEdit] = useState({
        name: '',
        price: '',
        image: ''
    });


    useEffect(() => {
        dispatch(getOne(id));
    }, [dispatch, id]);

    useEffect(() => {
        setEdit(oneDish);
    }, [oneDish]);

    const onChangeDish = (e) => {
        const {name, value} = e.target;
        setEdit(prevState => ({
            ...prevState,
            [name]: value
        }))
    }

    const saveHandler = (e) => {
        e.preventDefault()
        dispatch(editDish(id, edit, props.history))
    }

    return (
        edit && <>
            <Form
                onchange={onChangeDish}
                post={edit}
                onsubmit={(e) => saveHandler(e)}
            />
        </>
    );
};

export default EditDishes;