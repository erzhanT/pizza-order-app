import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchDishes, removeDishes} from "../../store/actions/dishesAction";
import Dish from "../../components/Dish/Dish";
import './Dishes.css';
import {NavLink} from "react-router-dom";

const Dishes = () => {

    const dispatch = useDispatch();
    const dishes = useSelector(state => state.dishes.dishes);


    useEffect(() => {
        dispatch(fetchDishes());
    }, [dispatch]);



    return (
        <div className="Dishes">
            <NavLink to={'/add'}>ADD DISH</NavLink>
            {dishes.map(dish => {
                return (
                    <Dish
                        id={dish.id}
                        key={dish.id}
                        name={dish.name}
                        price={dish.price}
                        image={dish.image}
                        remove={removeDishes(dish.id)}
                    />
                )
            })}
        </div>
    );
};

export default Dishes;