import React from 'react';

const Form = (props) => {
    return (
        <div>
            <h2>Dish:</h2>
            <form onSubmit={props.onsubmit}>
                <label>Name:</label>
                <br/>
                <input
                    type="text"
                    name='name'
                    value={props.post.name}
                    onChange={props.onchange}
                    required
                />
                <br/>
                <label>Price:</label>
                <br/>
                <input
                    type="text"
                    name='price'
                    value={props.post.price}
                    onChange={props.onchange}
                    required
                />
                <br/>
                <label>Image:</label>
                <br/>
                <input
                    type="text"
                    name='image'
                    value={props.post.image}
                    onChange={props.onchange}
                    required
                />
                <button>Add</button>
            </form>
        </div>
    );
};

export default Form;