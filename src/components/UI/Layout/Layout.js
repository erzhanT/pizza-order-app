import React from 'react';
import {AppBar, CssBaseline, Grid, Toolbar, Typography} from "@material-ui/core";
import {NavLink} from "react-router-dom";
import './Layout.css'

const Layout = ({children}) => {
    return (
        <>
            <CssBaseline />
            <AppBar position="fixed">
                <Toolbar>
                    <Grid container justify="space-between">
                        <Grid item>
                            <Typography variant="h6" noWrap>
                                Food Delivery
                            </Typography>
                        </Grid>
                        <Grid item>
                            <NavLink className='link' to="/" color="inherit">DISHES</NavLink>
                            <NavLink className='link' to="/orders" color="inherit">ORDERS</NavLink>
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
            <main>
                {children}
            </main>

        </>
    );
};

export default Layout;