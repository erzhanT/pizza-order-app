import axios from 'axios';

const url = 'https://restaurant-erzhan-default-rtdb.firebaseio.com/dishes.json'

export const FETCH_DISHES_SUCCESS = 'FETCH_DISHES_SUCCESS';
export const ADD_DISH = 'ADD_DISH';
export const REMOVE_DISH = 'REMOVE_DISH';
export const FETCH_ONE_DISH = 'FETCH_ONE_DISH';


export const fetchDishesSuccess = dishes => ({type: FETCH_DISHES_SUCCESS, dishes});
export const addDish = id => ({type: ADD_DISH, id});
export const removeDish = id => ({type: REMOVE_DISH, id});
export const fetchOneDish = data => ({type: FETCH_ONE_DISH, data})


export const fetchDishes = () => {
    return async dispatch => {
        try {
            const response = await axios.get(url);
            const dishes = Object.keys(response.data).map(id => ({
                ...response.data[id],
                id
            }));
            console.log(dishes);
            dispatch(fetchDishesSuccess(dishes));
        } catch (e) {
            console.error(e);
        }
    }
}
export const addDishes = (id, history) => {
    return async dispatch => {
        try {
            await axios.post(url, id);
            dispatch(addDish(id));
        } finally {
            history.push('/');
        }
    }
}

export const removeDishes = (id, history) => {
    return async dispatch => {
        try {
            await axios.delete('https://restaurant-erzhan-default-rtdb.firebaseio.com/dishes/' + id + '.json');
            // dispatch(removeDish(id))
        } catch (e) {
            console.error(e);
        }
    }
}

export const getOne = id => {
    return async dispatch => {
        try {
             const response = await axios.get('https://restaurant-erzhan-default-rtdb.firebaseio.com/dishes/' + id + '.json');
            console.log(response.data);
            dispatch(fetchOneDish(response.data));
        } catch (e) {
            console.error(e);
        }
    }
}
export const editDish = (id, data, history) => {
    return async dispatch => {
        try {
            await axios.put('https://restaurant-erzhan-default-rtdb.firebaseio.com/dishes/' + id + '.json', data);
        } finally {
            history.push('/')
        }
    }
}
