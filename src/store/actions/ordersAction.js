import axios from "axios";
import {fetchDishesSuccess} from "./dishesAction";

export const FETCH_ORDERS_SUCCESS = 'FETCH_ORDERS_SUCCESS';
export const REMOVE_ORDER = 'REMOVE_ORDER';

export const fetchOrdersSuccess = orders => ({type: FETCH_ORDERS_SUCCESS, orders});
export const RemoveOrder = id => ({type: REMOVE_ORDER, id});


const fetchOrders = () => {
    return async dispatch => {
        try {
            const response = axios.get();
            const orders = Object.keys(response.data).map(id => ({
                ...response.data[id],
                id
            }));
            dispatch(fetchDishesSuccess(orders));
        } catch (e) {
            console.error(e)
        }
    }
}
