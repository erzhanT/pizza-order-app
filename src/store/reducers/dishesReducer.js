import {FETCH_DISHES_SUCCESS, FETCH_ONE_DISH} from "../actions/dishesAction";

const initialState = {
    dishes: [],
    oneDish: null
};

export const dishesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_DISHES_SUCCESS :
            return {...state, dishes: action.dishes};
        case FETCH_ONE_DISH:
            return {...state, oneDish: action.data}
        default:
            return state;
    }
}