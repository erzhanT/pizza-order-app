import {FETCH_ORDERS_SUCCESS, REMOVE_ORDER} from "../actions/ordersAction";

const initialState = {
    items: {}
}

export const ordersReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ORDERS_SUCCESS:
            return {...state, items: action.items};
        case REMOVE_ORDER:
            return {...state, items: action.id}
        default:
            return state;
    }
}