import React from 'react';
import Layout from "./components/UI/Layout/Layout";
import {Switch, Route} from "react-router-dom";
import Dishes from "./containers/Dishes/Dishes";
import AddDishes from "./containers/AddDishes/AddDishes";
import Orders from "./containers/Orders/Orders";
import EditDishes from "./containers/EditDishes/EditDishes";

const App = () => (
    <Layout>
        <Switch>
            <Route path="/" exact component={Dishes}/>
            <Route path="/edit/:id" component={EditDishes}/>
            <Route path="/add" component={AddDishes}/>
            <Route path="/orders" component={Orders}/>
        </Switch>
    </Layout>
);

export default App;
